import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilesModule } from 'app/project/file-page/files.module';
import { MdProgressSpinnerModule } from '@angular/material';
import { APIService } from 'app/project/model/api.service';
import { AuthService } from 'app/project/model/auth.service';
import { AuthGuard } from 'app/project/model/auth-guard.service';

import { MainComponent } from 'app/project/main/main.component';
import { ConnexionDriveComponent } from 'app/project/connexion-drive/connexion-drive.component';
import { StaticPageComponent } from 'app/project/static-page/static-page.component';
import { PagenotfoundComponent } from 'app/project/pagenotfound/pagenotfound.component';

const appRoutes: Routes = [
  { path: 'home', component: MainComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PagenotfoundComponent }
];


@NgModule({
  declarations: [
    ConnexionDriveComponent,
    StaticPageComponent,
    MainComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdProgressSpinnerModule,
    FilesModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [APIService, AuthService],
  entryComponents: [StaticPageComponent],
  bootstrap: [StaticPageComponent]
})
export class AppModule{
}
