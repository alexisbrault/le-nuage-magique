import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../model/api.service'
import { AuthService } from '../model/auth.service'

@Component({
  selector: 'app-connexion-drive',
  templateUrl: './connexion-drive.component.html',
  styleUrls: ['./connexion-drive.component.css']
})
/* 
 * Represent a connection component to a certain cloud service
 * Displayed on the home page
 */
export class ConnexionDriveComponent implements OnInit {

  @Input() name : string;
  /**
   * Represent weither the user is connected on this source or not
   * Indicates what to display on the HTML
   */
  public isConnected = false;
  private isConnecting = false;
  private ppURL;
  private userName;

  constructor(public api: APIService, public auth: AuthService) { }

  ngOnInit() {
    this.isConnecting = true;
    if(this.cookieConnected()){
      this.accountInfos();
    }
  }

  /*
  * Connection to a particular drive source
  */
  public connect() {
    this.auth.login(this.name);
  }

  public accountInfos(){
    this.api.getAccountInfos(this.name).subscribe(
      infos => {
        this.checkConnection(infos);
        this.isConnecting = false;
      },
      err => { this.isConnecting = false; }
    );
  }

  /**
   * Check if the client account is connected on the server
   */
  public checkConnection(infos){
    console.log(infos);
    let accountJson = infos;
    //Check at least if we get the account informations of the correct cloud account
    if(accountJson.source === this.name){
      this.isConnected = true;
      this.ppURL = accountJson.picture;
      this.userName = accountJson.name;
    }
  }

  public disconnect(){
    this.auth.disconnect(this.name).subscribe(
      res => {
        this.isConnected = false;
        this.ppURL = '';
        this.userName = '';
      },
      err => { console.error(err); }
    );
  }

  /**
   * Checks in the browser cookie if an account is connected
   * @return True if an account is connected for this cloud
   */
  private cookieConnected(): boolean{
    //TODO
    return true;
  }
}
