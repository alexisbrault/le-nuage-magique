import { Component, OnInit, ViewChild } from '@angular/core';
import { FileDrive } from '../model/FileDrive';
import { Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { APIService } from '../model/api.service';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { StaticPageComponent } from '../static-page/static-page.component';
import { FileItem, FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-file-page',
  templateUrl: './file-page.component.html',
  styleUrls: ['./file-page.component.css'],
})
/**
 * The component containing the file tree informations and file actions.
 * Displaying on the /files page.
 */
export class FilePageComponent implements OnInit {

  // POPUP MODALS //
  @ViewChild('renamemodal')
  renamemodal: ModalComponent;
  @ViewChild('movemodal')
  movemodal: ModalComponent;
  @ViewChild('deletemodal')
  deletemodal: ModalComponent;
  @ViewChild('uploadmodal')
  uploadmodal: ModalComponent;
  @ViewChild('newfoldermodal')
  newfoldermodal: ModalComponent;

  public currentFolder: FileDrive; //The current folder
  public fileList: Array<FileDrive>;
  public stackFolder: Array<FileDrive>; //The stack trace of file tree
  public selectedFile: FileDrive; //The current selected file
  private previousSelectedFileRowColor: string; //The color of the row of the previous selected file to retrieve it
  private googleFilter: boolean = true; //True when we want it to be displayed
  private dropboxFilter: boolean = true;
  private upload: FileList; // The chosen file when uploading a file
  private loading: boolean = true;
  private uploader:FileUploader = new FileUploader({}); //Empty FileUploader, no options yet
  private filesToUpload:File[];

  constructor(public api: APIService, private http: Http) { }

  ngOnInit() {
    this.fileList = new Array<FileDrive>();
    this.stackFolder = new Array<FileDrive>();
    this.currentFolder = new FileDrive("Home", "folder", 0, [{ "name":"Dropbox", "id":"", "parentId":"" }, { "name":"GoogleDrive", "id":"root", "parentId":"" }], false); //We always start the app from the root
    this.stackFolder.push(this.currentFolder);
    this.loading = true;

    this.updateFiles();
  }

  /**
   * Build the FileDrive list from the NuageFile list
   * @param files the NuageFile list given by the server
   */
  private buildFilesList(files) {
    this.fileList = new Array<FileDrive>();
    for (let file of files) {
      let newFile:FileDrive = new FileDrive(file.name, file.type, file.size, file.sources, file.isShared);
      this.fileList.push(newFile);
    }
  }

  private trackByFiles(index:number, file:FileDrive){
    return file.name;
  }

  /**
   * @Triggered : Rename button
   * Rename the selected file  by the name entered in the field
   * @Callback : Update file tree
   */
  private renameFile() {
    if (this.selectedFile) {
      let newFileName = (<HTMLInputElement>document.getElementById('new-file-name')).value;
      this.api.rename(this.selectedFile, newFileName).subscribe(
        rep => {
          this.selectedFile.name = rep[0].name;
        },
        err => { console.error(err); }
      );
      this.renamemodal.close();
    } else {
      console.error("NO SELECTED FILE!");
    }
  }

  /**
   * @Triggered : simple click
   * Select a file by clicking on it
   * @param the file to select
   */
  private selectFile(selected: FileDrive) {
    if (this.selectedFile) {
      if (selected.sources[0].id != this.selectedFile.sources[0].id) {
        this.unselect();
        let myFile = document.getElementById(selected.sources[0].id);
        //Color the new selected file's row
        this.previousSelectedFileRowColor = myFile.style.backgroundColor;
        myFile.style.backgroundColor = "#D6E8FA";
        //Update the current selected file
        this.selectedFile = selected;
      } else {
        this.unselect();
      }
    } else {
      let myFile = document.getElementById(selected.sources[0].id);
      //Color the new selected file's row
      this.previousSelectedFileRowColor = myFile.style.backgroundColor;
      myFile.style.backgroundColor = "#D6E8FA";
      //Update the current selected file
      this.selectedFile = selected;
    }
  }

  /**
   * Unselect the selected file
   */
  private unselect() {
    //Restore the normal color of the previous selected file's row
    if (this.selectedFile) { //Check if there is a selected row already
      let previousSelected = document.getElementById(this.selectedFile.sources[0].id);
      previousSelected.style.backgroundColor = this.previousSelectedFileRowColor;
      this.selectedFile = null;
    }
  }

  /**
   * @Triggered : double click
   * Go inside a folder selected, get all its childrens, started by double clicking the file
   * @param file The file to go in
   */
  private goInSelectedFolder(file: FileDrive) {
    //If the file is a folder
    if (file.type == "folder") {
      this.currentFolder = file;
      this.stackFolder.push(this.currentFolder); //Adding the folder we go in
      this.selectedFile = null; //There is no selected file anymore
      this.updateFiles();
    }
  }

  private getCurrentRoute(): string{
    let route:string = 'files';
    for(let i in this.stackFolder){
      route += "/:name"+i;
    }
    return route;
  }

  /**
   * @Triggered : Back arrow button
   * Return to the parent folder (above in the tree)
   */
  private goBack() {
    this.stackFolder.pop()
    this.currentFolder = this.stackFolder[this.stackFolder.length-1];
    this.updateFiles(); //Going back on the stack
  }

  /**
   * @Triggered : Settings icon
   * Go back to home page
   */
  private returnToSettings() {
    window.location.href = "http://localhost:4200/home";
  }

  /**
   * @Triggered : Delete icon
   * Remove the current selected file
   */
  private deleteSelectedFile() {
    if (this.selectedFile) {
      this.deleteFile(this.selectedFile);
      this.selectedFile = null;
    }
    this.deletemodal.close();
  }

  /**
   * @param fileToRemove : The file to remove from the current folder
   * Remove a file in the current folder
   * @Callback : Update file tree
   */
  private deleteFile(fileToRemove: FileDrive) {
    if (fileToRemove) {
      this.api.removeFile(fileToRemove).subscribe(
        rep => {
          this.updateFiles();
        },
        err => {
          console.log(err);
        });
    }
  }

  /**
   * @Triggered : Move icon
   * Move a file from its location to another
   * Use the path given in the field
   * @Callback : Update file tree
   */
  private moveFile() {
    if (this.selectedFile) {
      let newPath = (<HTMLInputElement>document.getElementById('new-path')).value;
      console.log("Moving \'" + this.selectedFile.name + "\' to : " + newPath);
      this.api.moveFile(this.selectedFile, newPath).subscribe(
        rep => {
          this.updateFiles();
        },
        err => { console.log(err); }
      );
      this.movemodal.close();
      /* TODO : Use the checkboxes but display only the possible ones
         (if a file comes only from Dropbox, there is also the Google Drive checkbox... */
    } else {
      console.log("NO SELECTED FILE!");
    }
  }

  /**
   * @Triggered : New folder icon
   * Create an empty folder in the current location
   * Use the name given in the field
   * @Callback : Update file tree
   */
  private addingNewFolder() {
    let newname = (<HTMLInputElement>document.getElementById("newfolder")).value;

    //Get the absolute path of the current location
    let path = this.getFileStackString() + newname;
    console.log("Adding new empty folder : " + path);

    if ((<HTMLInputElement>document.getElementById("googleDriveNewFolder")).checked) {
      let name = path.split("/");
      this.api.newFolder(newname, "GoogleDrive", this.currentFolder.sources[0].parentId).subscribe(
        rep => { this.updateFiles(); },
        err => {
          console.log(err);
        });
    }
    if ((<HTMLInputElement>document.getElementById("dropboxNewFolder")).checked) {
      this.api.newFolder(path, "Dropbox", null).subscribe(
        rep => { this.updateFiles(); },
        err => {
          console.log(err);
        })

    }
    this.newfoldermodal.close();
  }

  // === UPLOAD ===================

  private onFilesChange(event){
    console.log(event);
    this.filesToUpload = event.srcElement.files;
  }

  /**
   * @Triggered : Upload button on upload modal
   * Upload the selected file on the upload popup
   * @Callback : Update file tree
   */
  private uploadFiles() {
    if(this.filesToUpload && this.filesToUpload.length > 0){
      if ((<HTMLInputElement>document.getElementById("googleDriveUpload")).checked) {
        let GDFileUploader:FileUploader = new FileUploader(this.api.getUploadOptions("GoogleDrive"));
        this.uploadAll(GDFileUploader);
      }
      if ((<HTMLInputElement>document.getElementById("dropboxUpload")).checked) {
        console.log(this.getFileStackString());
        let DBFileUploader:FileUploader = new FileUploader(this.api.getUploadOptions("Dropbox", {path: this.getFileStackString() }));
        this.uploadAll(DBFileUploader);
      }
    }

    this.uploadmodal.close();
  }

  /**
   * Upload all the selected files within a given FileUploader
   */
  private uploadAll(uploader:FileUploader){
    uploader.onProgressAll = (progress) => { console.log('Progress:' + progress); };
    uploader.onCompleteAll = () => { console.log("Upload completed."); };
    uploader.addToQueue(this.filesToUpload);
    uploader.uploadAll();
  }

  /** UTILS **/

  /**
  * @param parent The folder to look in. Equals to the current folder by default
   * @Triggered : Component initialization + file updating actions
   * Fetch all the remote files and update the file tree
   */
  private updateFiles(parent: FileDrive = this.currentFolder) {
    this.fileList = new Array<FileDrive>();
    this.selectedFile = null;
    this.api.getFiles(parent.sources).subscribe(
      files => { this.buildFilesList(files); this.loading = false; },
      err => { console.log(err); }
    );
  }

  /**
   * Return true if the file must be displayed
   */
  private filterFile(file: FileDrive): boolean {
    let res = false;
    for (let src of file.sources) {
      if (src.name === "GoogleDrive") {
        res = (res || this.googleFilter);
      } else if (src.name === "Dropbox") {
        res = (res || this.dropboxFilter);
      }
    }
    return res;
  }

  public formatBytes(a, b) {
    if (isNaN(a)) {
      return;
    }
    if (0 == a) {
      return "0 Bytes";
    }
    let c = 1e3,
      d = b || 2,
      extensions = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      level = Math.floor(Math.log(a) / Math.log(c));
    return parseFloat((a / Math.pow(c, level)).toFixed(d)) + " " + extensions[level]
  }

  public getFileStackString(): string {
    let path:string = "/";
    // Get the absolute path of the current location
    // [0] is always the root folder, we don't show it
    for (let i = 1; i < this.stackFolder.length; i++) {
      path += this.stackFolder[i].name + "/";
      console.log(i+":"+this.stackFolder[i].name);
    }
    // Add the current folder, which is not in the parent stack
    if(this.currentFolder.name != 'Home'){
      path += this.currentFolder.name + "/";
    }
    return path;
  }
}