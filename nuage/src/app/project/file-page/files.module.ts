import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { APIService } from 'app/project/model/api.service';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { MdProgressSpinnerModule } from '@angular/material';

import { FilePageComponent } from 'app/project/file-page/file-page.component';
import { StorageComponent } from 'app/project/storage/storage.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { AuthGuard } from 'app/project/model/auth-guard.service';

const filesRoutes: Routes = [
  {
    path: 'files',
    component: FilePageComponent,
    canActivate: [AuthGuard]
  }
];


@NgModule({
  declarations: [
    FilePageComponent,
    StorageComponent,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Bs3ModalModule,
    BrowserAnimationsModule,
    MdProgressSpinnerModule,
    RouterModule.forChild(filesRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [APIService, AuthGuard],
  entryComponents: [FilePageComponent],
})
export class FilesModule{
}
