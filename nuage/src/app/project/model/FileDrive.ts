export class FileDrive {

	public fileIcon = "assets/ic_insert_drive_file_black_24dp_1x.png";
	public folderIcon = "assets/ic_folder_black_24dp_1x.png";

	/**
	 * sources json format : { "name":"<GoogleDrive|Dropbox>", "id":"<id>" }
	 */
	constructor(public name : string, public type : string,
				public size:number, public sources, public isShared:boolean) {
	}

	public getIconURL():string{
		if(this.type == "folder"){
			return this.folderIcon;
		} else {
			return this.fileIcon;
		}
	}

	public toString():string{
		return "{" +
			"name:" + this.name + "," +
			"type:" + this.type + "," +
			"size:" + this.size + "," +
			"isShared:" + this.isShared + "," +
			"sources:" + JSON.stringify(this.sources) + "" +
			"}";
	}
}
