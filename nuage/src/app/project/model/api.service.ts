import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FileDrive } from '../model/FileDrive';
import { FileUploaderOptions } from 'ng2-file-upload';
import { CloudService } from '../model/cloud.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class APIService extends CloudService {

	constructor(protected http: Http){
		super();
	}

	/**
	 * Get the json file describing the entire file tree for all cloud
	 */
	public getFiles(sources){
		console.log("Reaching "+this.nuageUrl+"listFilesMerged");

		let formData:FormData = new FormData();
	    formData.append('src', sources);
	    let headers:Headers = new Headers();
	    headers.append('Content-Type', 'application/json');
	    let options:RequestOptions = new RequestOptions(headers);

		return this.http.post(this.nuageUrl+"listFilesMerged", sources, options)
						.map(this.handleResponse)
						.catch(this.handleError);
	}

	/**
	 * Get the json file describing the space usage for each cloud source
	 */
	public getSpaceUsage(){
		console.log("Reaching "+this.nuageUrl+"spaceUsage/");
		return this.http.get(this.nuageUrl+"spaceUsage/")
                  .map(this.handleResponse)
                  .catch(this.handleError);
	}

	/**
	 * Get the json file describing the profil information for each connected cloud
	 */
	public getAccountInfos(connectorName: string){
		console.log("Reaching "+this.nuageUrl+"accountInfos/" + connectorName);
		return this.http.get(this.nuageUrl+"accountInfos/" + connectorName)
                  .map(this.handleResponse)
                  .catch(this.handleError);
	}


	/**
	 * Call the server to rename the specified file
	 * Get a json file containing the server response (fail or success)
	 * It renames the file on all sources
	 * @param file The file to rename
	 * @param newName The new file name
	 */
	public rename(file:FileDrive, newName:string): Observable<any[]>{
		let requests = new Array();
		for(let i=0;i<file.sources.length;i++){
			let url:string = this.nuageUrl;
			let src = file.sources[i];
			url += "rename/" + src.name + "?fileid="+src.id+"&name="+newName;
			console.log("Reaching "+url);
			requests[i] = this.http.get(url)
	                  .map(this.handleResponse)
	                  .catch(this.handleError);
		}
		return Observable.forkJoin(requests);
	}

	/**
	 * Call the server to delete the specified file
	 * Get a json file containing the server response (fail or success)
	 * @param file The file to remove
	 */
	public removeFile(file:FileDrive): Observable<any[]>{
		let requests:any[] = new Array();
		// Remove the file on all its sources
		for(let i=0;i<file.sources.length;i++){
			let url:string = this.nuageUrl;
			let src = file.sources[i];
			url += "delete/" + src.name + "?id="+src.id;
			console.log("Reaching "+url);
			requests[i] = this.http.get(url)
	                  .map(this.handleResponse)
	                  .catch(this.handleError);
		}
		return Observable.forkJoin(requests);
	}

	/**
	 * Call the server to move a file to another folder
	 * Get a json file containing the server response (fail or success)
	 * @param file The file to rename
	 * @param path The path to the new folder
	 */
	public moveFile(file:FileDrive, path:string){
		let requests:any[] = new Array();
		for(let i=0;i<file.sources.length;i++){
			let url:string = this.nuageUrl;
			let src = file.sources[i];
			if(src.name === "GoogleDrive"){
				url += "move/GoogleDrive?from_path="+src.id+"&to_path="+path;
			} else if (src.name === "Dropbox" ){
				url += "move/Dropbox?from_path="+src.id+"&to_path="+path;
			}

			console.log("Reaching "+url);
			requests[i]=this.http.get(url)
	                  .map(this.handleResponse)
	                  .catch(this.handleError);
		}
		return Observable.forkJoin(requests);
	}

	/**
	 * Send the specified file to the server
	 * Get a json file containing the server response (fail or success)
	 * @param file The file to rename
	 * @param to The targeted cloud (GoogleDrive or Dropbox)
	 */
	public uploadFile(fileToUpload:File, to:string){
		let formData:FormData = new FormData();
	    formData.append('uploadFile', fileToUpload, fileToUpload.name);
	    let headers:Headers = new Headers();
	    headers.append('Content-Type', 'multipart/form-data');
	    headers.append('Accept', 'application/json');
	    let options:RequestOptions = new RequestOptions(headers);

		return this.http.post("http://localhost:8080/upload/"+to, formData, options)
            .map(this.handleResponse)
            .catch(this.handleError)
	}

	/**
	 * Tell the server to create an empty folder
	 * Get a json file containing the server response (fail or success)
	 * @param path The path of the new folder
	 * @param to The targeted cloud (GoogleDrive or Dropbox)
	 * @param idparent The parent file's id to indicate where to create the folder
	 */
	public newFolder (path:string, to:string, idparent:string) {
		let url:string = this.nuageUrl;
		if(to=="GoogleDrive"){
			url += "addNewFolder/"+to+"?name="+path+"&idparent="+idparent;
		}
		else{
			url += "addNewFolder/"+to+"?path="+path;
		}
		console.log("Reaching "+url);
		return this.http.get(url)
                  .map(this.handleResponse)
                  .catch(this.handleError);
	}

	/* UTILS */

	public getUploadOptions(to: string, additionnalOptions?:any): FileUploaderOptions{
		if(!additionnalOptions){
			additionnalOptions = {};
		}
		additionnalOptions.sources = to;
		let options:FileUploaderOptions = {
		    autoUpload: false,
		    isHTML5: true,
		    removeAfterUpload: false,
		    disableMultipart: false,
		    url: "http://localhost:8080/upload/"+to,
		    itemAlias: 'fileup',
		    additionalParameter: additionnalOptions
		  };

		return options;
	}

}
