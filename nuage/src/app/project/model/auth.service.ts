import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CloudService } from '../model/cloud.service';

@Injectable()
export class AuthService extends CloudService {

	constructor(private http: Http){
		super();
	}

	public login(cloud: string){
		window.open("http://localhost:8080/connect/" + cloud, '_self');
	}

	public disconnect(cloud: string){
		console.log("Reaching " + this.nuageUrl + "disconnect/" + cloud);
		return this.http.get(this.nuageUrl + "disconnect/" + cloud)
					.map(this.handleResponse)
					.catch(this.handleError)
	}
}
