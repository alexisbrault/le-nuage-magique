import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export abstract class CloudService {

	protected nuageUrl = 'http://localhost:8080/';

	constructor(){

	}
	/**
	 * Get a JSON answer from server
	 */
	protected handleResponse (res: Response) {
    	let response  = res.json();
    	// If there is a result code (success | error)
    	if(response.result){
    		CloudService.showSnackBar(response.result.code, response.result.message);
    	}
    	return response;
	}

	/**
	 * Print an error when the request failed
	 */
  	protected handleError (error: Response | any) {
    	let errMsg:string;
    	if (error instanceof Response) {
    		const response = error.json() || '';
    		// Is an error thrown by the server
	    	if(response.result){
	    		CloudService.showSnackBar(response.result.code, response.result.message);
	    		errMsg = response.result.message;
	    	} else {
		    	const err = response.error || JSON.stringify(response);
		    	errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
			    console.error(errMsg);
	    	}
	     } else {
	    	errMsg = error.message ? error.message : error.toString();
	    }
	    return Observable.throw(errMsg);
	}

	public static showSnackBar (code, msg) {
		// Display a response toast
		let snackBar = document.getElementById("snackbar");
		let snackMsg = document.getElementById("snackbarMsg");
		let snackImg = document.getElementById("snackbarImg");

		// Add the "show" class to DIV
		snackBar.className = "show";
		snackImg.className = code;
		snackMsg.innerHTML = msg;

		 // After 3 seconds, remove the show class from DIV
		setTimeout(function(){ snackBar.className = snackBar.className.replace("show", ""); }, 3000);
	}
}
