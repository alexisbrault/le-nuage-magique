var querystring = require('querystring');

var NuageConst = require("./nuageConst");
var NuageUtil = require("./nuageUtil");
var NuageFile = require("./nuageFile");
var NuageUsage = require("./nuageUsage");
var NuageAccount = require("./nuageAccount");

var fs = require('fs');
var streamifier = require("streamifier");
var readline = require('readline');
var Dropbox = require('dropbox');

const dropboxV2Api = require('dropbox-v2-api');

var CLIENT_ID = null;
var CLIENT_SECRET = null;
var credentials = null;
var account_id = null;
var dbx = null;
var isConnected = false;
const REDIRECT_URI = 'http://localhost:8080/authDropbox';

const name = 'Dropbox';

class DropboxConnector {
	constructor() {
		// Load the informations for connecting to the dropbox api
		this.readClientInfos();
	}

	get isConnected(){ return isConnected; }
	/**
     * Non static getter for outside calls
     */
    getName() { return name; }
    /**
     * Static getter for inside calls without bind(this)
     */
	static get name() { return name; }

	// === DROPBOX CONNEXION =========================

	readClientInfos() {
		// Load client secrets from a local file.
		fs.readFile('dropbox-credentials.json', function processClientSecrets(err, content) {
			if (err) {
				console.log('Error loading dropbox client secret file: ' + err);
				return;
			}
			credentials = JSON.parse(content);
			CLIENT_ID = credentials.client_id;
			CLIENT_SECRET = credentials.client_secret;

			dbx = dropboxV2Api.authenticate({
			    client_id: CLIENT_ID,
			    client_secret: CLIENT_SECRET,
			    redirect_uri: REDIRECT_URI
			});
		});
	}

	getConnexionURL() {
		var authUrl = dbx.generateAuthUrl();
		return authUrl;
	}

	getToken(code, res) {
		if (!!code) {
			dbx.getToken(code, (err, response) => {
				if(err){
			    	console.log(err);
			    	NuageUtil.error(res, "[Sign in] An error occured while connecting to dropbox api");
				} else {
				    account_id = response.account_id;
				    isConnected = true;
		       		res.status(301).redirect(NuageConst.URL_AFTER_CONNECT);
				}
			});
		} else {
			console.error("No code provided");
		}
	}

	disconnect(res) {
		if (isConnected) {
			isConnected = false;
			NuageUtil.success(res, "Dropbox account correctly disconnected");
		} else {
			console.error("The googledrive connector is already disconnected.");
			NuageUtil.error(res, "[Sign out] The dropbox connector is already disconnected.");
		}
	}

    // === FUNCTIONS ================================================

	account_infos(res, mainCallback) {
		dbx({
		    resource: 'users/get_account',
		    parameters: {
		        'account_id': account_id
		    }
		}, (err, result) => {
			if(err){
				console.error("[Account Infos] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while getting the Dropbox account informations");
			} else {
				let accountJson = new NuageAccount(DropboxConnector.name, result.name.display_name, result.email, result.profile_photo_url);
				NuageUtil.success(res, accountJson);
			}
		});
	}
	
	space_usage(res, mainCallback) {
		dbx({
		    resource: 'users/get_space_usage'
		}, (err, result) => {
			if(err){
				console.error("[space Usage] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while getting the space usage for Dropbox");
			} else {
				let storageJson = new NuageUsage(DropboxConnector.name, result.used, result.allocation.allocated);
				NuageUtil.success(res, storageJson);
			}
		});
	}

	files(path, res, mainCallback) {
		// If no path is provided : root folder
		if(!path){
			path = '';
		}
		dbx({
		    resource: 'files/list_folder',
		    parameters: {
		        'path': path,
		        'recursive': false,
		        'include_media_info': false,
		        'include_deleted': false,
		        'include_has_explicit_shared_members': false
		    }
		}, (err, result) => {
			if(err){
				console.error("[File List] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while getting the files for folder " + getFileNameFromPath(path));
			} else {
				let fileList = [];
				this.extractFiles(result, res, mainCallback, fileList);
			}
		});
	}

	/** 
	 * Get the next paginated files from the folder when the api can't give all in once
	 * @param cursor The cursor given by the previous list_folder request
	 */
	continue_files(cursor, res, mainCallback, fileList) {
		dbx({
		    resource: 'files/list_folder/continue',
		    parameters: {
		        'cursor': cursor
		    }
		}, (err, result) => {
		  	if(err){
				console.error("[Continue File List] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while getting the rest of the files");
			} else {
				this.extractFiles(result, res, mainCallback, fileList);
			}
		});
	}

	/**
	 * @param data the describing json for the file list given by dropbox api
	 * @param fileList the list of the NuageFile objects
	 */
	extractFiles(data, res, mainCallback, fileList) {
		for (let i = 0; i < data.entries.length; i++) {
			let file = data.entries[i];
			let nuageFile = this.createNuageFile(file);
			fileList.push(nuageFile);
		}
		// If there is more files
		if (data.has_more) {
			this.continue_files(data.cursor, res, mainCallback, fileList);
		} else { // End of the file list for this folder
			NuageUtil.success(res, fileList);
		}
	}

	create_newFolder(path, res, mainCallback) {
		dbx({
		    resource: 'files/create_folder_v2',
		    parameters: {
		        'path': path,
		        'autorename': false
		    }
		}, (err, result) => {
		     if(err){
				console.error(err);
				NuageUtil.sendError(res, "An error occured while getting the rest of the files");
			} else {
				let ret = this.createNuageFolder(result.metadata);
				NuageUtil.sendSuccess(res, "Folder successfully created", ret);
			}
		});
	}

	move(from_path, to_path, res, mainCallback) {
		dbx({
		    resource: 'files/move_v2',
		    parameters: {
		        'from_path': from_path,
		        'to_path': to_path,
		        'allow_shared_folder': false,
		        'autorename': false,
		        'allow_ownership_transfer': false
		    }
		}, (err, result) => {
			if(err){
				console.error("[Move File] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while moving the file " + getFileNameFromPath(from_path));
			} else {
				let ret = this.createNuageFile(result.metadata);
				NuageUtil.sendSuccess(res, "Folder successfully moved", ret);
			}
		});
	}

	rename(path, name, res, mainCallback) {
		this.move(path, this.getPathWithoutFileName(path) + "/" + name, res, mainCallback);
	}

	upload(file, filename, path, res, mainCallback) {

		dbx({
		    resource: 'files/upload',
		    parameters: {
		        path: path + filename
		    },
		    readStream: streamifier.createReadStream(file)
		}, (err, result) => {
			if(err){
				console.error("[Uplaod File] The API returned an error : " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured while upload the file");
			} else {
				console.log("[Upload File] It's okay");
			}
		});

		// fs.createReadStream(path).pipe(stream);

		// var data = {
		// 	"path": path + "/" + filename,
		// 	"mode": "add",
		// 	"autorename": true,
		// 	"mute": false
		// }

		// var options = {
		// 	host: 'content.dropboxapi.com',
		// 	path: '/2/files/upload',
		// 	headers: {
		// 		'Content-Type': 'application/octet-stream',
		// 		'Authorization': "Bearer " + this.bearer,
		// 		'Dropbox-API-Arg': JSON.stringify(data)
		// 	},
		// 	method: 'POST',
		// 	port: 443
		// };
		// return NuageUtil.getHttpRequest(data, options, NuageUtil.rep, res, mainCallback);
	}

	delete(path, res, mainCallback) {
		dbx({
		    resource: 'files/delete_v2',
		    parameters: {
		        'path': path
		    }
		}, (err, result) => {
			let ret = {};
		    if(err){
				console.error("[Delete File] The API returned an error: " + JSON.stringify(err));
				NuageUtil.sendError(res, "An error occured when deleting the file " + this.getFileNameFromPath(path));
			} else {
				NuageUtil.sendSuccess(res, "File successfully deleted", ret);
			}
		});
	}


	// === UTILS =============================

	/**
	 * @param dbfile a describing json for dropbox file
	 * @return a NuageFile object
	 */
	createNuageFile(dbFile){
		let nuageFile = new NuageFile(dbFile.name);
		nuageFile.type = dbFile['.tag'];
		nuageFile.size = dbFile.size;
		let parentPath = this.getPathWithoutFileName(dbFile.path_display);
		let src = {
			name: DropboxConnector.name,
			id: dbFile.path_display,
			parentId: parentPath
		};
		nuageFile.sources.push(src);
		nuageFile.isShared = (typeof dbFile.sharing_info != 'undefined');
		return nuageFile;
	}

	/**
	 * @param dbfile a describing json for dropbox file
	 * @return a NuageFile object
	 */
	createNuageFolder(dbFolder){
		let nuageFile = new NuageFile(dbFolder.name);
		nuageFile.type = 'folder';
		let parentPath = this.getPathWithoutFileName(dbFolder.path_display);
		let src = {
			name: DropboxConnector.name,
			id: dbFolder.path_display,
			parentId: parentPath
		};
		nuageFile.sources.push(src);
		nuageFile.isShared = (typeof dbFolder.sharing_info != 'undefined');
		return nuageFile;
	}

	getPathWithoutFileName(path){
		return path.split('/').slice(0,-1).join('/');
	}

	getFileNameFromPath(path){
		return path.split('/').pop();
	}

}

module.exports = DropboxConnector;
