var querystring = require('querystring');

var NuageConst = require("./nuageConst");
var NuageUtil = require("./nuageUtil");
var NuageFile = require("./nuageFile");
var NuageUsage = require("./nuageUsage");
var NuageAccount = require("./nuageAccount");

var fs = require('fs');
var streamifier = require("streamifier");
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

var drive = null;
var credentials = null;
var oauth2Client = null;
isConnected = false;
var SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.metadata',
    'https://www.googleapis.com/auth/drive.metadata.readonly'
];

const name = 'GoogleDrive';

class GoogleDriveConnector {

    constructor() {
        // Load the informations for connecting to the google api
        this.readClientInfos();
    }

    get isConnected(){ return isConnected; }
    /**
     * Non static getter for outside calls
     */
    getName() { return name; }
    /**
     * Static getter for inside calls without bind(this)
     */
    static get name() { return name; }

    // === GOOGLE DRIVE CONNEXION =========================

    readClientInfos() {
        // Load client secrets from a local file.
        fs.readFile('drive-credentials.json', function processClientSecrets(err, content) {
            if (err) {
                console.log('Error loading googledrive client secret file: ' + err);
                return;
            }
            credentials = JSON.parse(content);
            //Create the OAuth2
            var clientSecret = credentials.web.client_secret;
            var clientId = credentials.web.client_id;
            var redirectUrl = credentials.web.redirect_uris[0];
            var auth = new googleAuth();
            oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
            drive = google.drive({ version: 'v3', auth: oauth2Client });
        });
    }

    /**
     * Get the url to autorize the app with the client's google account
     */
    getConnexionURL() {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES
        });
        return authUrl;
    }

    /**
     * Retrieve a token for a single user who authorized the application
     */
    getToken(code, res) {
        oauth2Client.getToken(code, function(err, tokens) {
            if (err) {
                console.error('Error while trying to retrieve access token', err);
                NuageUtil.error(res, "[Sign in] An error occured while connecting to googledrive api");
            } else {
                isConnected = true;
                oauth2Client.setCredentials(tokens);
                res.status(301).redirect(NuageConst.URL_AFTER_CONNECT);
            }
        });
    }

    disconnect(res) {
        if (isConnected) {
           isConnected = false;
           NuageUtil.success(res, "GoogleDrive account correctly disconnected");
        } else {
            console.error("The googledrive connector is already disconnected.");
            NuageUtil.error(res, "[Sign out] The googledrive connector is already disconnected.");
        }
    }

    // === FUNCTIONS ================================================

    account_infos(res, mainCallback) {
        drive.about.get({
            auth: oauth2Client,
            fields: "user"
        }, function(err, response) {
            if (err) {
                console.log('[Account Infos] The API returned an error: ' + JSON.stringify(err));
                NuageUtil.sendError(res, "An error occured while getting the GoogleDrive account informations");
            } else {
                let accountJson = new NuageAccount(GoogleDriveConnector.name, response.user.displayName, response.user.emailAddress, response.user.photoLink);
                NuageUtil.success(res, accountJson);
            }
        });
    }

    space_usage(res, mainCallback) {
        drive.about.get({
            auth: oauth2Client,
            fields: "storageQuota"
        }, function(err, response) {
            if (err) {
                console.log('[Space Usage] The API returned an error: ' + JSON.stringify(err));
                NuageUtil.sendError(res, "An error occured while getting the space usage for GoogleDrive");
            } else {
                let storageJson = new NuageUsage(GoogleDriveConnector.name, data.storageQuota.usage, data.storageQuota.limit);
                NuageUtil.success(res, storageJson);
            }
        });
    }

    /**
     * Get the google drive files for the given folder
     * @param {integer} parentId The id of the folder we want to get the files
     * @param res The initial client request
     * @param mainCallback The callback method we call after retrieving the files
     */
    files(parentId, res, mainCallback) {
        // If no parentId is provided : root folder
        if(!parentId){
            parentId = 'root';
        }
        let q = '';

        if (parentId != ('' || null || undefined)) {
            q = "'" + parentId + "' in parents";
        }

        drive.files.list({
            'auth': oauth2Client,
            'fields': 'nextPageToken, files(id, name, parents, description, mimeType, iconLink, createdTime, shared, size)',
            'q': q
        }, function(err, response) {
            if (err) {
                console.log('[File List] The API returned an error: ' + JSON.stringify(err));
            } else {
                let files = response.files;
                this.extractFiles(files, res, mainCallback);
            }
        }.bind(this));
    }

    /**
     * Write the json for files in our own format
     * @param data A json representing a GoogleDrive file
     */
    extractFiles(gdFiles, res, mainCallback) {
        let nuageFilesJson = [];
        for (var fileIndex = 0; fileIndex < gdFiles.length; fileIndex++) {
            let file = gdFiles[fileIndex];
            nuageFilesJson.push(this.createNuageFile(file));
        }

        NuageUtil.success(res, nuageFilesJson);
    }

    create_newFolder(name, idParent, res, mainCallback) {
        let fileMetadata;
        if (idParent != '') { // If a parent id is given
            fileMetadata = {
                "name": name,
                "mimeType": "application/vnd.google-apps.folder",
                "parents": [idParent]
            };
        } else { // The folder goes to root if not
            fileMetadata = {
                "name": name,
                "mimeType": "application/vnd.google-apps.folder",
            };
        }
        drive.files.create({
            resource: fileMetadata,
            fields: 'kind, id, name, parents, description, mimeType, iconLink, createdTime, shared, size'
        }, function(err, file) {
            if (err) {
                console.log('[Create Folder] The API returned an error: ' + JSON.stringify(err));
                NuageUtil.sendError(res, "An error occured while creating a new folder");
            } else {
                NuageUtil.sendSuccess(res, "Folder successfully created", this.createNuageFile(file));
            }
        });
    }

    rename(id, name, res, mainCallback) {
        let fileMetadata = {
            "name": name
        };

        drive.files.update({
            fileId: id,
            resource: fileMetadata,
            fields: 'id, name, parents'
        }, function(err, file) {
            if (err) {
                console.error('[Rename File] The API returned an error: ' + JSON.stringify(err));
                NuageUtil.sendError(res, "An error occured while renaming the file");
            } else {
                // POJO representing a file in the app
                let nuageFile = new NuageFile(file.name);
                // The sources of the file
                let sources = {
                    name: GoogleDriveConnector.name,
                    id: file.id,
                    parentId: file.parents[0]
                };
                nuageFile.sources.push(sources);

                NuageUtil.sendSuccess(res, "File successfully renamed", this.createNuageFile(file));
            }
        });
    }

    move(id, newIdParent, oldIdParent, res, mainCallback) {
        drive.files.get({
            fileId: fileId,
            fields: 'parents'
        }, function(err, file) {
            if (err) {
                console.error("[Get file's parents] The API returned an error: " + JSON.stringify(err));
            } else {
                // Move the file to the new folder
                var previousParents = file.parents.join(',');
                drive.files.update({
                    fileId: fileId,
                    addParents: newIdParent,
                    removeParents: previousParents,
                    fields: 'id, parents'
                }, function(err, file) {
                    if (err) {
                        console.error('[Move File] The API returned an error: ' + JSON.stringify(err));
                        NuageUtil.sendError(res, "An error occured while moving the file");
                    } else {
                        NuageUtil.sendSuccess(res, "File successfully moved", this.createNuageFile(file));
                    }
                });
            }
        });
    }

    delete(id, res, mainCallback) {
        drive.files.delete({
                fileId: id,
                fields: 'id, name'
            },
            function(err, nothing) {
                if (err) {
                    console.error('[Delete File] The API returned an error: ' + JSON.stringify(err));
                    NuageUtil.sendError(res, "An error occured while deleting the file");
                } else {
                    NuageUtil.sendSuccess(res, "An error occured while moving the file", nothing);
                }
            }
        );
    }

    //TODO : Update
    upload(file, filename, mimetype, res, mainCallback) {
        let fileMetadata = {
            "name": filename,
        };

        let media = {
			mimeType: mimetype,
			body: streamifier.createReadStream(file)
		};

        drive.files.create({
            // uploadType: "media",
            media: media,
            resource: fileMetadata,
            fields: 'id, name, parents'
        }, function(err, file) {
            if (err) {
                console.error('[Upload File] The API returned an error: ' + JSON.stringify(err));
                NuageUtil.sendError(res, "An error occured while uploading the file");
            } else {
            	NuageUtil.sendSuccess(res, "File " + filename + " successfully uploaded");
            }
        });
    }

    afterUpload(data, res, mainCallback, filename) {
        let json = JSON.parse(data);

        data = JSON.stringify({
            "name": filename
        });

        var options = {
            host: 'www.googleapis.com',
            path: '/drive/v3/files/' + json.id,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data),
                'Authorization': 'Bearer ' + this.bearer
            },
            method: 'PATCH',
            port: 443
        };
        NuageUtil.httpRequest(data, options, NuageUtil.rep, res, mainCallback);
    }

        /**
     * @param dbfile a describing json for dropbox file
     * @return a NuageFile object
     */
    createNuageFile(gdFile){
        // POJO representing a file in the app
        let nuageFile = new NuageFile(gdFile.name);
        // File format
        let kind = gdFile.mimeType == 'application/vnd.google-apps.folder' ? 'folder' : 'file';
        // The type of the file
        nuageFile.type = kind;
        // File size in byte
        nuageFile.size = gdFile.size;
        // If the file is shared with other users
        nuageFile.isShared = gdFile.shared;
        // The source of the file
        let src = {
            name: GoogleDriveConnector.name,
            id: gdFile.id,
            parentId: gdFile.parents[0] //TODO : Attention, one day we may want to have all the parents!
        };
        nuageFile.sources.push(src);
        return nuageFile;
    }

}

module.exports = GoogleDriveConnector;