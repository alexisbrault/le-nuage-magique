var shortid = require('shortid');

class NuageFile {
	constructor(name) {
		this.sources = [];
		this.size = 0;
		this.name = name;
		this.type = '';
		this.id = shortid.generate();
		this.isShared = false;
		this.parentId = '';
	}

	toJson(){
		var res = {
	        "name": this.name,
	        "id": this.id,
			"size": this.size,
    	    "type": this.type,
    	    "isShared": this.isShared,
    	    "parentId": this.parentId
		}
		res.sources = this.sources;
		return res;
	}
}

module.exports = NuageFile;
