class NuageUsage {
	constructor(source, used, total) {
		this.source = source;
		this.used = used;
		this.total = total;
	}

	toJson(){
		var res = {
			"source": this.source,
	        "used": this.used,
	        "total": this.total
		}
		return res;
	}
}

module.exports = NuageUsage;
