var https = require('https');

class NuageUtil {

	// --- Not used anymore --- //

	// static httpRequest(data, options, callback, response, mainCallback) {
	// 	var req = https.request(options, function(res) {
	// 		res.setEncoding('utf8');
	// 		let content = '';
	// 		res.on('data', function(chunk) {
	// 			content += chunk;
	// 		});
	// 		res.on('end', function() {
	// 			if (res.statusCode === 200 || res.statusCode === 204) {
	// 				if (typeof callback === 'undefined')
	// 					console.log(content);
	// 				else
	// 					callback(content, response, mainCallback);
	// 			} else {
	// 				console.log('Status:', res.statusCode);
	// 				console.log(content);
	// 				NuageUtil.error(response, res.statusCode);
	// 			}
	// 		});
	// 	}).on('error', function(err) {
	// 		NuageUtil.error(response, err);
	// 	});;

	// 	if (typeof data === 'undefined') {

	// 	} else {
	// 		req.write(data);
	// 	}
	// 	req.end()
	// }

	// static httpRequestFiles(data, options, callback, response, mainCallback, fileList) {
	// 	var req = https.request(options, function(res) {
	// 		res.setEncoding('utf8');
	// 		let content = '';
	// 		res.on('data', function(chunk) {
	// 			content += chunk;
	// 		});
	// 		res.on('end', function() {
	// 			if (res.statusCode === 200 || res.statusCode === 204) {
	// 				if (typeof callback === 'undefined')
	// 					console.log(content);
	// 				else
	// 					callback(content, response, mainCallback, fileList);
	// 			} else {
	// 				console.log('Status:', res.statusCode);
	// 				console.log(content);
	// 				NuageUtil.error(response, res.statusCode);
	// 			}
	// 		});
	// 	}).on('error', function(err) {
	// 		NuageUtil.error(response, err);
	// 	});;

	// 	if (typeof data === 'undefined') {

	// 	} else {
	// 		req.write(data);
	// 	}
	// 	req.end()
	// }

	// static getHttpRequest(data, options, callback, response, mainCallback, filename) {
	// 	let req = https.request(options, function(res) {
	// 		res.setEncoding('utf8');
	// 		let content = '';
	// 		res.on('data', function(chunk) {
	// 			content += chunk;
	// 		});
	// 		res.on('end', function() {
	// 			if (res.statusCode === 200 || res.statusCode === 204) {
	// 				if (typeof callback === 'undefined')
	// 					console.log(content);
	// 				else
	// 					callback(content, response, mainCallback, filename);
	// 			} else {
	// 				console.log('Status:', res.statusCode);
	// 				console.log(content);
	// 				NuageUtil.error(response, res.statusCode);
	// 			}
	// 		});
	// 	}).on('error', function(err) {
	// 		NuageUtil.error(response, err);
	// 	});

	// 	return req;
	// }

	/**
	 * Send a response, doesn't trigger the toast
	 * @param res the express response
	 * @param json the json data to send to client
	 */
	static success(res, json) {
		res.end(JSON.stringify(json));
	}

	/**
	 * Send an error response, doesn't trigger the toast
	 * @param res the express response
	 * @param json the json data to send to client
	 */
	static error(res, json) {
		res.status(500).end(JSON.stringify(json));
	}

	/**
	 * Send a success response, triggers the toast
	 * @param res the express response
	 * @param msg the error message
	 * @param json the json data to send to client
	 */
	static sendSuccess(res, msg, json) {
		// If no data is passed
		if(!json){ json = {}; }
		// If there is data but stringified, we parse it to json
		if ( typeof json === "string" ){
			json = JSON.parse(json);
		}
		json.result = {
			'code': 'success',
			'message': msg
		};
		res.end(JSON.stringify(json));
	}

	/**
	 * Send an error response without any data, triggers the toast
	 * @param res the express response
	 * @param msg the error message
	 */
	static sendError(res, msg) {
		let ret = {
			'code': 'error',
			'message': msg
		};
		res.status(500).end(JSON.stringify(ret));
	}
}

module.exports = NuageUtil;
