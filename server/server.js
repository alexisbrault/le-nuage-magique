var fs = require('fs');
var express = require('express');
var rand = require("generate-key");
var session = require('client-sessions');
var https = require('https');
var simpleBarrier = require('simple-barrier');
var busboy = require('connect-busboy');
var multer = require('multer');
var url = require('url');
var bodyParser = require("body-parser");

var NuageUtil = require("./nuageUtil");
var NuageConst = require("./nuageConst");


// === CONNECTORS =================================================

var GoogleDriveConnector = require("./googleDriveConnector");
var DropboxConnector = require("./dropboxConnector");

var app = express();

var allowCrossDomain = function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
	res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	res.setHeader('Access-Control-Allow-Credentials', true);

	// intercept OPTIONS method
	if ('OPTIONS' == req.method) {
		res.sendStatus(200);
	} else {
		displayFullUrl(req);
		next();
	}
};
var DIR = "./test/";
var storage = multer.memoryStorage()
var upload = multer({
		storage: storage
	});

app.use(busboy());
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(multer({
// 		dest: DIR,
// 		rename: function (fieldname, filename) {
// 			return filename + Date.now();
// 		},
// 		onFileUploadStart: function (file) {
// 			console.log(file.originalname + ' is starting ...');
// 		},
// 		onFileUploadComplete: function (file) {
// 			console.log(file.fieldname + ' uploaded to  ' + file.path);
// 		}
// 	})
// );
app.use(session({
	cookieName: 'NuageMagique',
	secret: 'zCfyZjuhaSQR7M3v7bkD',
	duration: 30 * 60 * 1000,
	activeDuration: 5 * 60 * 1000,
}));


connectorList = [];
GDC = new GoogleDriveConnector();
DBC = new DropboxConnector();
connectorList.push(GDC);
connectorList.push(DBC);

app.get('/', function(req, res) {
	fs.readFile('wiki.html', function(err, content) {
		if (err) {
			console.log('Error loading wiki file: ' + err);
			return;
		}
		res.end(content);
	});
});

// === CONNEXION ==================================================

app.get('/connect/Dropbox', function(req, res) {
	res.status(302).redirect(DBC.getConnexionURL());
});

app.get('/connect/GoogleDrive', function(req, res) {
	res.status(302).redirect(GDC.getConnexionURL());
});

// === AUTHENTIFICATION ===========================================

app.get('/authDropbox', function(req, res) {
	DBC.getToken(req.query.code, res);
});

app.get('/authGoogleDrive', function(req, res) {
	GDC.getToken(req.query.code, res);
});

// === DISCONNEXION ===============================================

app.get('/disconnect/:connectorName', function(req, res) {
	let connectorName = req.params.connectorName;
	let connector = getConnectorByName(connectorList, connectorName);
	connector.disconnect(res);
	// if (connector === null){
	// 	NuageUtil.sendError(res, connectorName + " account is not connected");
	// }
	// connectorList.splice(connectorList.indexOf(connector), 1);
	// NuageUtil.sendSuccess(res, connectorName + " account correctly disconnected");
});

// === ACCOUNT INFORMATIONS =======================================

app.get('/accountInfos/:connectorName', function(req, res) {
	let connectorName = req.params.connectorName;
	let connector = getConnectorByName(connectorList, connectorName);
	if (connector.isConnected){
		connector.account_infos(res, writeOutJSON);
	} else {
		NuageUtil.error(res, "No " + connectorName + " account is connected.");
		console.error("The connector " + connectorName + " is not connected");
	}
});

// === LIST FILES =================================================

/**
 * @body The body is the json of the folder's sources:
 * 		'src': [ { 'name':<connector name>, 'id': <folder id>, 'parentId': <folder parent id> } ]
 * (parentId is not used yet)
 */
app.post('/listFilesMerged', function(req, res) {
	let sources = req.body;
	if (connectorList.length === 0){
		NuageUtil.sendError(res, 'No cloud connected');
	}

	let barrier = simpleBarrier();
	// For each source, we get the files by the correct api (connector)
	for (let i = 0; i < sources.length; i++) {
		let src = sources[i];
		// Get the connector dynamically by his name
		let connector = getConnectorByName(connectorList, src.name);
		if(connector.isConnected){
			// Get the file list for this folder
			connector.files(src.id, res, barrier.waitOn( (res, data) => data) );
		}
	}

	//When all the list files had been retrieved
	barrier.endWith(function(fileListsJson) {
		if(fileListsJson.length == 1){
			writeOutJSON(res, fileListsJson[0]);
		} else {
			// We merge the json by pair.
			// Each json represents a file list from a connector
			let json1 = fileListsJson[0]; //json1 is the json receiving all the other files
			for (var i = 0; i < fileListsJson.length-1; i++) {
				merge(json1, fileListsJson[i + 1]);
			}
			writeOutJSON(res,json1);
		}
	});
});

function merge(json1, json2) {
	// If there is only one json, we can't merge.
	if(!json2) return json1;

	let temp = []
	for (let i = 0; i < json1.length; i++) {
		let file1 = json1[i];
		for (let j = 0; j < json2.length; j++) {
			let file2 = json2[j];
			if (file1.name === file2.name) {
				file1.sources = file1.sources.concat(file2.sources);
				// merge(file1.children, file2.children);
				file2.added = true;
			}
		}
	}

	// Add to json1 the files not in common (not marked by added=true)
	for (let j = 0; j < json2.length; j++) { //Only not added file
		let file2 = json2[j];
		if (!file2.added)
			json1.push(file2);
	}
}

/* V2 */
app.get('/listFiles/:connectorName/:parentId', function(req, res) {
	let connectorName = req.params.connectorName;
	let parentId = req.params.parentId;
	console.log('/listFiles/' + connectorName + ' called');
	let connector = getConnectorByName(connectorList, connectorName);
	if (connector === null){
		NuageUtil.sendError(res, 'No cloud connected');
	}
	connector.files(parentId, res, writeOutJSON);
});

function mergelistFiles(res, data) {
	return data;
}

// === SPACE USAGE ================================================

app.get('/spaceUsage', function(req, res) {
	if (connectorList.length === 0){
		NuageUtil.sendError(res, 'No cloud connected');
	}

	let barrier = simpleBarrier();
	for (var i = 0; i < connectorList.length; i++) {
		connectorList[i].space_usage(res, barrier.waitOn(mergeSpaceUsage));
	}
	barrier.endWith(function(json) {
		console.log('/spaceUsage ok');
		res.end(JSON.stringify(json));
	});
});

/* V2 */
app.get('/spaceUsage/:connectorName', function(req, res) {
	let connectorName = req.params.connectorName;
	console.log('/spaceUsage/' + connectorName + ' called');
	let connector = getConnectorByName(connectorList, connectorName);
	if (connector.isConnected){
		connector.space_usage(res, writeOutJSON);
	} else {
		NuageUtil.sendError(res, connectorName + " account not connected.");
	}
});

function mergeSpaceUsage(res, data) {
	return data;
}

// === DELETE FILE ================================================

app.get('/delete/:connectorName', function(req, res) {
	let connector = getConnectorByName(connectorList, req.params.connectorName);
	if (connector.isConnected){
		connector.delete(req.query.id, res, writeOutJSON);
	} else {
		NuageUtil.sendError(res, connectorName + " account not connected.");
	}
});

// === NEW FOLDER =================================================

app.get('/addNewFolder/Dropbox', function(req, res) {
	DBC.create_newFolder(req.query.path, res, writeOutJSON);
});

app.get('/addNewFolder/GoogleDrive', function(req, res) {
	GDC.create_newFolder(req.query.name, req.query.idparent, res, writeOutJSON);
});


// === MOVE =======================================================

app.get('/move/Dropbox', function(req, res) {
	DBC.move(req.query.from_path, req.query.to_path, res, writeOutJSON);
});

app.get('/move/GoogleDrive', function(req, res) {
	GDC.move(req.query.id, req.query.newIdParent, req.query.oldIdParent, res, writeOutJSON);
});


// === UPLOAD =====================================================

app.post('/upload/Dropbox', upload.single('fileup'), function(req, res) {
	let uploadedFile = req.file;
	DBC.upload(uploadedFile.buffer, uploadedFile.originalname, req.body.path, res, writeOutJSON);
});

app.post('/upload/GoogleDrive', upload.single('fileup'), function(req, res) {
	let uploadedFile = req.file;
	GDC.upload(uploadedFile.buffer, uploadedFile.originalname, uploadedFile.mimetype, res, writeOutJSON);
});

// === RENAME =====================================================

app.get('/rename/:connectorName', function(req, res) {
	let connector = getConnectorByName(connectorList, req.params.connectorName);
	if (connector.isConnected){
		let file = req.query.fileid;
		let newname = req.query.name;
		connector.rename(file, newname, res, writeOutJSON);
	} else {
		NuageUtil.sendError(res, connectorName + " account not connected.");
	}
});

// === UTILS ===========================================

/**
 * Callback function to send back a Json to client side
 */
function writeOutJSON(res, data) {
	res.end(JSON.stringify(data));
}

/**
 * Get a cloud connector by his name in the list
 */
function getConnectorByName(connectorList, name) {
	// console.log("Searching for " + name)
	for (let i = 0; i < connectorList.length; i++) {
		// console.log("Connector: " + connectorList[i].getName());
		if (connectorList[i].getName() === name){
			return connectorList[i];
		}
	}
	return null;
}

function displayFullUrl(req){
    let time = new Date();
	console.log("[" + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + "] " + req.originalUrl);
}

// === MAIN ===================================================

app.listen(8080, function() {
	console.log('*** MagicCloud\'s server is running on port 8080! ***');
});
